import com.ib.client.EClientSocket;
import com.ib.client.EReader;
import com.ib.client.EReaderSignal;

import Test2.MyEwrapperInterface;

public class Dummy2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EClientSocket m_client ;
		  
			MyEwrapperInterface wrapper = new MyEwrapperInterface();
			
			  m_client = (EClientSocket) wrapper.getClient();
			   final EReaderSignal m_signal = wrapper.getSignal();
			
			   m_client.eConnect("127.0.0.1",7497,2);
	         final EReader reader = new EReader(m_client, m_signal);   
			
			   reader.start();
			    //An additional thread is created in this program design to empty the messaging queue
			    new Thread(() -> {
			     while (m_client.isConnected()) {
			      m_signal.waitForSignal();
			      try {
			       reader.processMsgs();
			       } catch (Exception e) {
			        System.out.println("Exception: "+e.getMessage());
			        }
			    }
			  }).start();
			   
			    m_client.reqAccountUpdates(true, "DU744275");   
			    m_client.reqAccountUpdates(false, "DU744275");
	}

}
