import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ib.client.EClientSocket;
import com.ib.client.EReader;
import com.ib.client.EReaderSignal;

import Test2.MyEwrapperInterface;
import jersey.repackaged.com.google.common.collect.Table.Cell;


public class dummy {
	static File fileName = new File("C:\\Users\\Finflock-3\\Desktop\\temp\\Fund.xlsx");

	public static void main(String arrgs[]) {
		System.out.println("--");
		try {
			createWorkbook(fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void createWorkbook(File fileName) throws IOException {
	    try {
	        FileOutputStream fos = new FileOutputStream(fileName);
	        XSSFWorkbook  workbook = new XSSFWorkbook();            

	        XSSFSheet sheet = workbook.createSheet("UpdateAccountValue");  
	      
	        Row row = sheet.createRow(0);   
	        org.apache.poi.ss.usermodel.Cell cell0 = row.createCell(0);
	        cell0.setCellValue("key");

	        org.apache.poi.ss.usermodel.Cell cell1 = row.createCell(1);

	        cell1.setCellValue("value");       

	        org.apache.poi.ss.usermodel.Cell cell2 = row.createCell(2);
	        cell2.setCellValue("currency");
	        
	        org.apache.poi.ss.usermodel.Cell cell3= row.createCell(3);
	        cell3.setCellValue("AccountName");

	        
	        workbook.write(fos);
	        fos.flush();
	        fos.close();
	    } catch (FileNotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}

}
